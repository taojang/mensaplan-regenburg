enyo.kind({
	name: "MensaplanRegensburg",
	kind: enyo.VFlexBox,
	components: [
		{kind: "Scrim", layoutKind: "VFlexLayout", align: "center", pack: "center", components: [
			{kind: "SpinnerLarge"}
		]},
		{name: "topBar", kind: "PageHeader", layoutKind: "HFlexLayout", className: "enyo-header-dark", components: [
			//{kind:"Button", caption: "Test", className:"enyo-button-dark", onclick: "test"},
			{kind: "Button", caption: $L("Back"), className: "enyo-button-dark", onclick: "backHandler"},
			{kind: "VFlexBox", flex:1, align: "center", components:[
				{name:"viewTitle", content: ""}
			]}
			
			
		]},
		{name: "slidingPane", kind: "SlidingPane", flex: 1, onSelectView: "onSelectViewHandler", components: [
			{name: "left", width: "320px", kind:"SlidingView", title: $L("Select Cafeteria"),components: [
					{kind: "Scroller", flex: 1, components: [
						//Insert your components here
						{kind: "VirtualRepeater", onSetupRow: "setupMensaRow",components: [
							{kind: "Item", onclick: "mensaClickHandler", tapHighlight: true, components: [
								{name: "mensaName"}
							]}
						]},
					]},
					{kind: "Toolbar", components: [
						{kind: "GrabButton"}
					]}
			]},
			{name: "right", kind:"SlidingView", flex: 1, components: [
					{kind: "Scroller", name: "detailScroller", flex: 1, components: [
						//Insert your components here
						
					]},
					{kind: "Toolbar", components: [
						{kind: "GrabButton"},
						{
							kind: "ToolButtonGroup",
							components: [
								{
									name: "prevDayButton",
									icon: "images/menu-icon-back.png",
									className: "enyo-radiobutton-dark enyo-grouped-toolbutton-dark",
									onclick: "prevDayHandler"
								},
								{
									name: "todayButton",
									width: "80px",
									caption: $L("Today"),
									className: "enyo-radiobutton-dark enyo-grouped-toolbutton-dark",
									onclick: "todayHandler"
								},
								{
									name: "nextDayButton",
									icon: "images/menu-icon-forward.png",
									className: "enyo-radiobutton-dark enyo-grouped-toolbutton-dark",
									onclick: "nextDayHandler"
								}
							]
						}
					]}
			]},
			
		]},
		// web service to get csv data
		{
			name: "getData",
			kind: "WebService",
			handleAs: "json",
			onSuccess: "gotData",
			onFailure: "gotDataFailure"
		}
	],
	create: function() {
		this.inherited(arguments);
		this.basePipeUrl = "http://pipes.yahoo.com/pipes/pipe.run?_id=b7be217dccd1891c0afbca249a9b9f47&_render=json&filename=";
		this.csvList =	[
			['Uni Mensa - Mittags', 'r_uni_'],
			['Uni-Cafeteria PT - Mittags', 'r_pt_'],
			['Hochschul-Mensa Seybothstr. - Mittags', 'r_fh_m_'],
			['Hochschul-Mensa Seybothstr. - Abends', 'r_fh_a_'],
			['Hochschul-Mensa Prüfeningerstr. - Mittags', 'r_fh_p_']
		];
		this.dayofWeek = ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'];
		this.$.viewTitle.setContent(this.$.slidingPane.getView().title);
		this.currDayIndex = 0;
	},
	test: function() {
		this.$.getData.setUrl(this.basePipeUrl + this.csvList[0][1]);
		this.$.getData.call();
		this.$.slidingPane.selectView(this.$.right);

		
	},
	backHandler: function(inSender, e) {
		if (this.$.slidingPane.getViewIndex() > 0) {
			this.$.slidingPane.back(e);
			
		}
	},
	onSelectViewHandler: function(inSender, inView, inPreviousView) {
		if (inView.title)
			this.$.viewTitle.setContent(inView.title);
	},
	gotData: function(inSender, inResponse) {
		var responseContent = inResponse.value.items;
		var dateGroup = [];
		// cleanup the product indicators
		for (itemIndex in responseContent) {
			// skip the description item
			if (responseContent[itemIndex].preis === undefined)
				continue;
			switch(responseContent[itemIndex].warengruppe[0]) {
				case 'H':
					responseContent[itemIndex].warengruppe = "Hauptgerichte";
					break;
				case 'B':
					responseContent[itemIndex].warengruppe = "Beilagen";
					break;
				case 'S':
					responseContent[itemIndex].warengruppe = "Suppen";
					break;
				case 'N':
					responseContent[itemIndex].warengruppe = "Nachspeisen";
					break;
				default:
					responseContent[itemIndex].warengruppe = "N/A";
			}
		}
		
		// group the data after date
		for (itemIndex in responseContent) {
			// filter the description item out
			if (responseContent[itemIndex].preis === undefined)
				continue;
			var found = false;
			for (groupIndex in dateGroup) {
				if (responseContent[itemIndex].datum == dateGroup[groupIndex][0]) {
					dateGroup[groupIndex][1].push(responseContent[itemIndex]);
					found = true;
					break;
				}
			}
			if (found === true)
				continue;
			dateGroup.push([responseContent[itemIndex].datum, [responseContent[itemIndex]]]);
		}
		this.dateGroup = dateGroup;
		// dateGroup = [['date', [items], ['product group', [items]]]]
		
		//this.$.test.setContent(JSON.stringify(this.dateGroup));

		// continue to group the product
		for (dayIndex in this.dateGroup) {
			// initialize array
			if (this.dateGroup[dayIndex][2] === undefined)
				this.dateGroup[dayIndex][2] = []; // initialize
			
			for (itemIndex in this.dateGroup[dayIndex][1]) {
				var found = false; // reset found flag
				for(productIndex in this.dateGroup[dayIndex][2]) {
					if (this.dateGroup[dayIndex][2][productIndex][0] == this.dateGroup[dayIndex][1][itemIndex].warengruppe) {
						this.dateGroup[dayIndex][2][productIndex][1].push(this.dateGroup[dayIndex][1][itemIndex]);
						found = true;
						break;
					}
						
				}
				if (found === true)
					continue;
				this.dateGroup[dayIndex][2].push([this.dateGroup[dayIndex][1][itemIndex].warengruppe, [this.dateGroup[dayIndex][1][itemIndex]]]);
			}
		}
		// set current day index to today's Index
		this.currDayIndex = this.getTodayIndex();
		// set Nav buttons
		this.setNavButtonStatus();
		//this.$.test.setContent(JSON.stringify(this.dateGroup[0][2]));
		//this.$.productGroupList.doSetupRow();
		this.createDrawers();
		// hide scrim
		this.$.scrim.setShowing(false);
		this.$.spinnerLarge.setShowing(false);
	},
	gotDataFailure: function(inSender, inResponse) {
		console.log("Error when fetching data");
	},

	getTodayIndex: function() {
		// determine today's index
		var d = new Date();
		var todayIndex;
		var todayDate = (d.getDate() < 10)? ('0'+ d.getDate()):d.getDate();
		var todayMonth = ((d.getMonth() + 1) < 10)?('0' + (d.getMonth() + 1)):(d.getMonth() + 1);
		var todayString =  todayDate+ '.'+ todayMonth+ '.'+ d.getFullYear();
		for (i in this.dateGroup) {
			if (this.dateGroup[i][0] == todayString) {
				return parseInt(i);
			}
		}
		return false;
	},

	getProductGroup: function(inSender, inIndex) {
		if (this.dateGroup === undefined)
			return;
		
		var dateIndex = this.currDayIndex;
		//console.log(dateIndex);
		try {
			var product = this.dateGroup[dateIndex][2][inIndex];
		}
		catch (e) {
			console.log(JSON.stringify(this.dateGroup));
		}
		if (product) {
			this.$.dividerDrawer.setCaption(product[0]);
			this.$.dividerDrawer.$.productIndex = inIndex;
			//alert(product[0]);
			return true;
		}
	},
	createDrawers: function() {
		if (this.$.productGroupList !== undefined)
			this.$.productGroupList.doSetupRow();
		if (this.$.productGroupList === undefined) {
			this.$.detailScroller.createComponent(
				{name: "productGroupList", kind: "VirtualRepeater", onSetupRow: "getProductGroup", flex: 1, components:[
					{kind: "DividerDrawer", open: true, components: [
						{kind: "VirtualRepeater", onSetupRow: "getCourses", components:[
							{kind: "Item", flex:1, layoutKind: "HFlexLayout", components:[
								{name: "name", flex: 1},
								{name: "price", flex:1, align: "end"}
							]}
						]}
					]}
				]}, {owner: this});
		}
		
		this.$.detailScroller.render();
	},
	getCourses: function(inSender, inIndex) {
		var dateIndex = this.currDayIndex;
		c = this.dateGroup[dateIndex][2][inSender.parent.parent.parent.$.productIndex][1][inIndex];
		//console.log(JSON.stringify(c));
		if (c) {
			this.$.name.setContent(c.name);
			this.$.price.setContent("Stud: " + c.stud + " € " + "Bed: " + c.bed + " € " + "Gast: " + c.gast + " €");
			if (inIndex == 0) {
				this.$.name.parent.addClass("enyo-first");
			}
			if (this.dateGroup[dateIndex][2][inSender.parent.parent.parent.$.productIndex][1][inIndex + 1] === undefined) {
				this.$.name.parent.addClass("enyo-last");
			}
			return true;
		}
	},

	setupMensaRow: function(inSender, inIndex) {
		var m = this.csvList[inIndex];
		if (m) {
			if (inIndex == 0) {
				this.$.mensaName.parent.addClass("enyo-first");
			}
			if (this.csvList[inIndex + 1] === undefined) {
				this.$.mensaName.parent.addClass("enyo-last");
			}
			this.$.mensaName.setContent(m[0]);
			return true;
		}
	},

	mensaClickHandler: function(inSender, inEvent) {
		// show Scrim
		this.$.scrim.setShowing(true);
		this.$.spinnerLarge.setShowing(true);
		this.$.getData.setUrl(this.basePipeUrl + this.csvList[inEvent.rowIndex][1]);
		this.$.getData.call();
		this.$.slidingPane.selectView(this.$.right);
		// set today's string as title
		this.$.viewTitle.setContent(new enyo.g11n.DateFmt({format: "EEE MMM d, yyyy"}).format(new Date()));
		// reset day navigate button
		this.setNavButtonStatus();
	},

	// helper function, generate localeDateString
	getDateFromString: function(s) {
		var datePieces = s.split(".");
		// original is dd.mm.yyyy, but date obj will take only mm.dd.yyyy
		var d = new Date();
		d.setTime(Date.parse(datePieces[1] + '.' + datePieces[0]+ '.' + '.' + datePieces[2]));
		//var localeDate = enyo.g11n.DateFmt({format: "EEE, MMM d, yyyy"});
		return new enyo.g11n.DateFmt({format: "EEE MMM d, yyyy"}).format(new Date(d.getTime()));
		
	},

	// set nav button status
	setNavButtonStatus: function() {
		if (this.currDayIndex === undefined || this.dateGroup === undefined) {
			// disable all
			this.$.prevDayButton.setDisabled(true);
			this.$.todayButton.setDisabled(true);
			this.$.nextDayButton.setDisabled(true);
		} else {
			this.$.prevDayButton.setDisabled(false);
			this.$.todayButton.setDisabled(false);
			this.$.nextDayButton.setDisabled(false);
			if (this.currDayIndex <= 0) {
				// disable prev
				this.$.prevDayButton.setDisabled(true);
			}

			if (this.dateGroup[this.currDayIndex + 1] === undefined) {
				// disable next
				this.$.nextDayButton.setDisabled(true);
			}
		}

		
	},

	setTitleToCurrDate: function() {
		var currDateString = this.dateGroup[this.currDayIndex][0];
		var localeDate = enyo.g11n.DateFmt()
		this.$.viewTitle.setContent(this.getDateFromString(currDateString));
			
	},
	
	prevDayHandler: function(inSender, inEvent) {
		// check if there is prev days data
		if (this.currDayIndex > 0) {
			this.currDayIndex -= 1;
			//console.log("curr Index: " + this.currDayIndex);
			// refresh list
			this.createDrawers();
			this.setTitleToCurrDate();
		}
		this.setNavButtonStatus();
	},

	todayHandler: function(inSender, inEvent) {
		// disable or enable the prev/next button
		this.currDayIndex = this.getTodayIndex();
		//console.log("today Index: " + this.getTodayIndex());
		//console.log("total length: " + this.dateGroup.length);
		this.setNavButtonStatus();
		this.setTitleToCurrDate();
		// refresh list
		this.createDrawers();
	},

	nextDayHandler: function(inSender, inEvent) {
		if (this.dateGroup[this.currDayIndex + 1] !== undefined) {
			this.currDayIndex += 1;
			//console.log("curr Index: " + this.currDayIndex);
			this.setTitleToCurrDate();
			this.createDrawers();
		}
		this.setNavButtonStatus();
	}
	
	
});
